package contact

// Name contains the first and last name of an individual
type Name struct {
	First, Last string
	full        string
}

// Full returns the full name stored in Name
func (n *Name) Full() string {
	if n.full == "" {
		if n.First != "" && n.Last != "" {
			n.full = n.First + " " + n.Last
		} else if n.First != "" {
			n.full = n.First
		} else if n.Last != "" {
			n.full = n.Last
		}
	}

	return n.full
}

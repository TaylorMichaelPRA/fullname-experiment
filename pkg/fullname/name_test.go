package contact

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

// Getter Method Tests

// TestNameFull tests the behavior of Full().
func TestNameFull(t *testing.T) {

	// An immediately-invoked function expression (IIFE) for testing if 2
	// objects are identical: actual ("got") vs. expected ("want").
	var check = func(got interface{}, want interface{}) {
		if !cmp.Equal(got, want) {
			t.Errorf("got %v, want %v", got, want)
		}
	}

	// An immediately-invoked function expression (IIFE) for standardizing a
	// test message prefix.
	var msg = func(condition string) string {
		return "get full name when: " + condition
	}

	// Generic field tests

	t.Run(msg("first and last names exist"), func(t *testing.T) {
		name := Name{
			First: "John",
			Last:  "Doe",
		}

		check(name.Full(), "John Doe")
	})

	t.Run(msg("only first name exist"), func(t *testing.T) {
		name := Name{
			First: "John",
			Last:  "",
		}

		check(name.Full(), "John")
	})

	t.Run(msg("only last name exist"), func(t *testing.T) {
		name := Name{
			First: "",
			Last:  "Doe",
		}

		check(name.Full(), "Doe")
	})

	t.Run(msg("neither first nor last names exist"), func(t *testing.T) {
		name := Name{
			First: "",
			Last:  "",
		}

		check(name.Full(), "")
	})

}
